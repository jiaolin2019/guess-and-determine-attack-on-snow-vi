#include<iostream>
#include <math.h>
#include<fstream>
#include<conio.h>
#include<stdio.h>
#include<malloc.h>
#include <time.h>
#include <vector>
using namespace std;
ifstream fin("pathco.txt");
ofstream fout("pathf.txt");

void readpath(int*** pathtable, int pathlen)
{
	for (int i = 0; i < pathlen; i++)
		for (int j = 0; j < 2; j++)
			for (int k = 0; k < 4; k++)
				fin >> pathtable[i][j][k];
	fin.close();
	/*for (int i = 0; i < pathlen; i++)
	{
		fout << i << " ";
		for (int j = 0; j < 2; j++)
		{
			fout << pathtable[i][j][0] << " ";
			if (pathtable[i][j][1] == 'R' + 1)
				fout << "R1 ";
			else if (pathtable[i][j][1] == 'R' + 2)
				fout << "R2 ";
			else if (pathtable[i][j][1] == 'R' + 3)
				fout << "R3 ";
			else
				fout << char(pathtable[i][j][1]) << " ";
			for (int k = 2; k < 4; k++)
				fout << pathtable[i][j][k] << " ";
		}
		fout << "\n";
	}*/
}
int BuildInitialIndexTable(int** A, int T, int** Equation)
{
	int count = 0;
	for (int j = 0; j < 16; j++)//E:Z_15-j
		for (int t = 0; t <= T; t++)
		{
			A[count][0] = 80 - j + 112 * t;//R1
			A[count][1] = 64 - j + 112 * t;//b
			A[count][2] = 96 - j + 112 * t;//R2
			Equation[count][0] = 'Z';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:Z " << 15 - j << " " << t << "\n";
			count++;
		}

	for (int j = 0; j < 4; j++)//E:R1_15 - j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - 4 * j + 112 * (t + 1);
			A[count][1] = 96 - j + 112 * t;
			A[count][2] = 112 - j + 112 * t;
			A[count][3] = 32 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j-4
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - (4 * j + 1) + 112 * (t + 1);
			A[count][1] = 96 - 4 - j + 112 * t;
			A[count][2] = 112 - 4 - j + 112 * t;
			A[count][3] = 32 - 4 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 4;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j - 4 << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j - 8
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - (4 * j + 2) + 112 * (t + 1);
			A[count][1] = 96 - 8 - j + 112 * t;
			A[count][2] = 112 - 8 - j + 112 * t;
			A[count][3] = 32 - 8 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 8;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j - 8 << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j - 12
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - (4 * j + 3) + 112 * (t + 1);
			A[count][1] = 96 - 12 - j + 112 * t;
			A[count][2] = 112 - 12 - j + 112 * t;
			A[count][3] = 32 - 12 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 12;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j - 12 << " " << t << "\n";
			count++;
		}

	for (int j = 0; j < 16; j++)//E:a_32-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 32 - j + 112 * (t + 1);
			A[count][1] = 48 - j + 112 * t;
			A[count][2] = 16 - j + 112 * t;
			A[count][3] = 30 - j + 112 * t;
			Equation[count][0] = 'a';
			Equation[count][1] = 31 - j;
			Equation[count][2] = t;
			//fout << "E:a " << 31 - j << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 16; j++)//E:a_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 16 - j + 112 * (t + 1);
			A[count][1] = 32 - j + 112 * t;
			Equation[count][0] = 'a';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:a " << 15 - j << " " << t << "\n";
			count++;
		}

	for (int j = 0; j < 16; j++)//E:b_32-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 64 - j + 112 * (t + 1);
			A[count][1] = 16 - j + 112 * t;
			A[count][2] = 48 - j + 112 * t;
			A[count][3] = 64 - j + 112 * t;
			Equation[count][0] = 'b';
			Equation[count][1] = 31 - j;
			Equation[count][2] = t;
			//fout << "E:b " << 31 - j << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 16; j++)//E:b_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 48 - j + 112 * (t + 1);
			A[count][1] = 64 - j + 112 * t;
			Equation[count][0] = 'b';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:b " << 15 - j << " " << t << "\n";
			count++;
		}

	for (int t = 0; t < T; t++)//E:R2_3
	{
		A[count][0] = 93 + 112 * (t + 1);
		A[count][1] = 94 + 112 * (t + 1);
		A[count][2] = 95 + 112 * (t + 1);
		A[count][3] = 96 + 112 * (t + 1);
		A[count][4] = 77 + 112 * t;
		A[count][5] = 66 + 112 * t;
		A[count][6] = 71 + 112 * t;
		A[count][7] = 76 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 3;
		Equation[count][2] = t;
		//fout << "E:R2 3 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_2
	{
		A[count][0] = 89 + 112 * (t + 1);
		A[count][1] = 90 + 112 * (t + 1);
		A[count][2] = 91 + 112 * (t + 1);
		A[count][3] = 92 + 112 * (t + 1);
		A[count][4] = 73 + 112 * t;
		A[count][5] = 78 + 112 * t;
		A[count][6] = 67 + 112 * t;
		A[count][7] = 72 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 2;
		Equation[count][2] = t;
		//fout << "E:R2 2 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_1
	{
		A[count][0] = 85 + 112 * (t + 1);
		A[count][1] = 86 + 112 * (t + 1);
		A[count][2] = 87 + 112 * (t + 1);
		A[count][3] = 88 + 112 * (t + 1);
		A[count][4] = 69 + 112 * t;
		A[count][5] = 74 + 112 * t;
		A[count][6] = 79 + 112 * t;
		A[count][7] = 68 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 1;
		Equation[count][2] = t;
		//fout << "E:R2 1 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_0
	{
		A[count][0] = 81 + 112 * (t + 1);
		A[count][1] = 82 + 112 * (t + 1);
		A[count][2] = 83 + 112 * (t + 1);
		A[count][3] = 84 + 112 * (t + 1);
		A[count][4] = 65 + 112 * t;
		A[count][5] = 70 + 112 * t;
		A[count][6] = 75 + 112 * t;
		A[count][7] = 80 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 0;
		Equation[count][2] = t;
		//fout << "E:R2 0 " << t << "\n";
		count++;
	}

	for (int t = 0; t < T; t++)//E:R3_3
	{
		A[count][0] = 109 + 112 * (t + 1);
		A[count][1] = 110 + 112 * (t + 1);
		A[count][2] = 111 + 112 * (t + 1);
		A[count][3] = 112 + 112 * (t + 1);
		A[count][4] = 93 + 112 * t;
		A[count][5] = 82 + 112 * t;
		A[count][6] = 87 + 112 * t;
		A[count][7] = 92 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 3;
		Equation[count][2] = t;
		//fout << "E:R3 3 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_2
	{
		A[count][0] = 105 + 112 * (t + 1);
		A[count][1] = 106 + 112 * (t + 1);
		A[count][2] = 107 + 112 * (t + 1);
		A[count][3] = 108 + 112 * (t + 1);
		A[count][4] = 89 + 112 * t;
		A[count][5] = 94 + 112 * t;
		A[count][6] = 83 + 112 * t;
		A[count][7] = 88 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 2;
		Equation[count][2] = t;
		//fout << "E:R3 2 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_1
	{
		A[count][0] = 101 + 112 * (t + 1);
		A[count][1] = 102 + 112 * (t + 1);
		A[count][2] = 103 + 112 * (t + 1);
		A[count][3] = 104 + 112 * (t + 1);
		A[count][4] = 85 + 112 * t;
		A[count][5] = 90 + 112 * t;
		A[count][6] = 95 + 112 * t;
		A[count][7] = 84 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 1;
		Equation[count][2] = t;
		//fout << "E:R3 1 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_0
	{
		A[count][0] = 97 + 112 * (t + 1);
		A[count][1] = 98 + 112 * (t + 1);
		A[count][2] = 99 + 112 * (t + 1);
		A[count][3] = 100 + 112 * (t + 1);
		A[count][4] = 81 + 112 * t;
		A[count][5] = 86 + 112 * t;
		A[count][6] = 91 + 112 * t;
		A[count][7] = 96 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 0;
		Equation[count][2] = t;
		//fout << "E:R3 0 " << t << "\n";
		count++;
	}

	for (int j = 0; j < 16; j++)//back_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 64 - j + 112 * (t + 1);
			A[count][1] = 32 - j + 112 * (t + 1);
			A[count][2] = 30 - j + 112 * t;
			A[count][3] = 64 - j + 112 * t;
			A[count][4] = 48 - j + 112 * t;
			A[count][5] = 16 - j + 112 * t;
			Equation[count][0] = 'B';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:back " << 15 - j << " " << t << "\n";
			count++;
		}

	/*for (int i = 0; i < count; i++)
	{
		fout << i << " ";
		for (int j = 0; j < 8; j++)
			fout << A[i][j] << " ";
		fout << "\n";
	}*/

	return count;
}

void correctab(int*** pathtable, int i, int eq, int pm, int pathlen, int** Record)
{
	int flag;
	if ((pathtable[i][0][1] == eq) && (pathtable[i][0][2] == pathtable[i][1][2] - 16) && (pathtable[i][0][3] == pathtable[i][1][3]))
	{
		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if (((pathtable[j][0][1] == pathtable[i][0][1]) && (pathtable[j][0][2] == pathtable[i][0][2]) && (pathtable[j][0][3] == pathtable[i][0][3])) || ((pathtable[j][2][1] == pathtable[i][0][1]) && (pathtable[j][2][2] == pathtable[i][0][2]) && (pathtable[j][2][3] == pathtable[i][0][3])) || ((Record[j][0] == pathtable[i][0][1]) && (Record[j][1] == pathtable[i][0][2]) && (Record[j][2] == pathtable[i][0][3])))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			pathtable[i][2][0]++;
			pathtable[i][2][1] = pathtable[i][0][1];
			pathtable[i][2][2] = pathtable[i][0][2];
			pathtable[i][2][3] = pathtable[i][0][3];
		}
		if ((pathtable[i][3][1] == pathtable[i][0][1]) && (pathtable[i][3][2] == pathtable[i][0][2]) && (pathtable[i][3][3] == pathtable[i][0][3]))
		{
			pathtable[i][3][0]++;
			pathtable[i][3][1] = 0;
			pathtable[i][3][2] = 0;
			pathtable[i][3][3] = 0;
		}

		Record[i][0] = pathtable[i][0][1];
		Record[i][1] = pathtable[i][0][2] + pm;
		Record[i][2] = pathtable[i][0][3];

		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if (((pathtable[j][0][1] == pathtable[i][0][1]) && (pathtable[j][0][2] == pathtable[i][0][2] + pm) && (pathtable[j][0][3] == pathtable[i][0][3])) || ((pathtable[j][2][1] == pathtable[i][0][1]) && (pathtable[j][2][2] == pathtable[i][0][2] + pm) && (pathtable[j][2][3] == pathtable[i][0][3])))
			{
				flag = 1;
				pathtable[i][3][0]--;
				pathtable[i][3][1] = pathtable[i][0][1];
				pathtable[i][3][2] = pathtable[i][0][2] + pm;
				pathtable[i][3][3] = pathtable[i][0][3];
			}
		}
		if (flag == 0)
		{
			for (int j = i + 1; j < pathlen; j++)
			{
				if ((pathtable[j][0][1] == pathtable[i][0][1]) && (pathtable[j][0][2] == pathtable[i][0][2] + pm) && (pathtable[j][0][3] == pathtable[i][0][3]))
				{
					pathtable[j][3][0]--;
					pathtable[j][3][1] = pathtable[i][0][1];
					pathtable[j][3][2] = pathtable[i][0][2] + pm;
					pathtable[j][3][3] = pathtable[i][0][3];
				}
			}
		}

	}
	else
	{
		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if (((pathtable[j][0][1] == eq) && (pathtable[j][0][2] == pathtable[i][1][2] - 16 + pm) && (pathtable[j][0][3] == pathtable[i][1][3])) || ((pathtable[j][2][1] == eq) && (pathtable[j][2][2] == pathtable[i][1][2] - 16 + pm) && (pathtable[j][2][3] == pathtable[i][1][3])) || ((Record[j][0] == eq) && (Record[j][1] == pathtable[i][1][2] - 16 + pm) && (Record[j][2] == pathtable[i][1][3])))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			pathtable[i][2][0]++;
			pathtable[i][2][1] = eq;
			pathtable[i][2][2] = pathtable[i][1][2] - 16 + pm;
			pathtable[i][2][3] = pathtable[i][1][3];
			for (int j = i + 1; j < pathlen; j++)
			{
				if ((pathtable[j][0][1] == eq) && (pathtable[j][0][2] == pathtable[i][1][2] - 16 + pm) && (pathtable[j][0][3] == pathtable[i][1][3]))
				{
					pathtable[j][3][0]--;
					pathtable[j][3][1] = eq;
					pathtable[j][3][2] = pathtable[i][1][2] - 16 + pm;
					pathtable[j][3][3] = pathtable[i][1][3];
				}
			}
		}

	}
}
void correctBack(int*** pathtable, int i, int pm, int pathlen, int** Record)
{
	int flag=0;
	for (int j = 0; j < i; j++)
	{
		if (((pathtable[j][0][1] == 'a') && (pathtable[j][0][2] == pathtable[i][1][2]  + pm) && (pathtable[j][0][3] == pathtable[i][1][3])) || ((pathtable[j][2][1] == 'a') && (pathtable[j][2][2] == pathtable[i][1][2] + pm) && (pathtable[j][2][3] == pathtable[i][1][3])) || ((Record[j][0] == 'a') && (Record[j][1] == pathtable[i][1][2] + pm) && (Record[j][2] == pathtable[i][1][3])))
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		pathtable[i][2][0]++;
		pathtable[i][2][1] = 'a';
		pathtable[i][2][2] = pathtable[i][1][2]  + pm;
		pathtable[i][2][3] = pathtable[i][1][3];
		for (int j = i + 2; j < pathlen; j++)
		{
			if ((pathtable[j][0][1] == 'a') && (pathtable[j][0][2] == pathtable[i][1][2]  + pm) && (pathtable[j][0][3] == pathtable[i][1][3]))
			{
				pathtable[j][3][0]--;
				pathtable[j][3][1] = 'a';
				pathtable[j][3][2] = pathtable[i][1][2] + pm;
				pathtable[j][3][3] = pathtable[i][1][3];
			}
		}
	}

	flag = 0;
	for (int j = 0; j < i; j++)
	{
		if (((pathtable[j][0][1] == 'b') && (pathtable[j][0][2] == pathtable[i][1][2] + pm) && (pathtable[j][0][3] == pathtable[i][1][3])) || ((pathtable[j][2][1] == 'b') && (pathtable[j][2][2] == pathtable[i][1][2] + pm) && (pathtable[j][2][3] == pathtable[i][1][3])) || ((Record[j][0] == 'b') && (Record[j][1] == pathtable[i][1][2] + pm) && (Record[j][2] == pathtable[i][1][3])))
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		pathtable[i + 1][2][0]++;
		pathtable[i + 1][2][1] = 'b';
		pathtable[i + 1][2][2] = pathtable[i][1][2] + pm;
		pathtable[i + 1][2][3] = pathtable[i][1][3];
		for (int j = i + 2; j < pathlen; j++)
		{
			if ((pathtable[j][0][1] == 'b') && (pathtable[j][0][2] == pathtable[i][1][2] + pm) && (pathtable[j][0][3] == pathtable[i][1][3]))
			{
				pathtable[j][3][0]--;
				pathtable[j][3][1] = 'b';
				pathtable[j][3][2] = pathtable[i][1][2] + pm;
				pathtable[j][3][3] = pathtable[i][1][3];
			}
		}
	}
}

void correctZR(int*** pathtable, int i, int pathlen)
{
	int flag;
	if ((pathtable[i][1][2] != 0) && (pathtable[i][1][2] != 4) && (pathtable[i][1][2] != 8) && (pathtable[i][1][2] != 12))
	{
		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if ((pathtable[j][1][1] == pathtable[i][1][1]) && (pathtable[j][1][2] == pathtable[i][1][2] - 1) && (pathtable[j][1][3] == pathtable[i][1][3]))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			pathtable[i][2][0]++;
			pathtable[i][2][1] = pathtable[i][1][1];
			pathtable[i][2][2] = pathtable[i][1][2] - 1;
			pathtable[i][2][3] = pathtable[i][1][3];
			for (int k = i + 1; k < pathlen; k++)
			{
				if ((pathtable[k][1][1] == pathtable[i][1][1]) && (pathtable[k][1][2] == pathtable[i][1][2] - 1) && (pathtable[k][1][3] == pathtable[i][1][3]))
				{
					if (pathtable[k][3][0] == 0)
					{
						pathtable[k][3][0]--;
						pathtable[k][3][1] = pathtable[i][1][1];
						pathtable[k][3][2] = pathtable[i][1][2] - 1;
						pathtable[k][3][3] = pathtable[i][1][3];
					}
					else
					{
						pathtable[k][3][0]--;
						pathtable[k][4][1] = pathtable[i][1][1];
						pathtable[k][4][2] = pathtable[i][1][2] - 1;
						pathtable[k][4][3] = pathtable[i][1][3];
					}
				}
			}
		}
	}


}

void correctpath(int*** pathtable, int pathlen)
{
	int** Record = (int**)calloc(sizeof(int*), pathlen);
	if (Record != NULL)
		for (int i = 0; i < pathlen; i++)
		{
			Record[i] = (int*)calloc(sizeof(int), 3);
			if (Record[i] == NULL)
				exit(0);
		}


	for (int i = 0; i < pathlen; i++)
	{
		if (pathtable[i][1][1] == 'a')
		{
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 1))
				correctab(pathtable, i, 'a', -1, pathlen, Record);
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 0))
				correctab(pathtable, i, 'a', 1, pathlen, Record);
		}
		if (pathtable[i][1][1] == 'b')
		{
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 1))
				correctab(pathtable, i, 'b', -1, pathlen, Record);
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 0))
				correctab(pathtable, i, 'b', 1, pathlen, Record);
		}
		if (pathtable[i][1][1] == 'B'&& pathtable[i+1][1][1] == 'B')
		{
			if (pathtable[i][1][2] % 2 == 1)
				correctBack(pathtable, i, -1, pathlen, Record);
			if  (pathtable[i][1][2] % 2 == 0)
				correctBack(pathtable, i,  1, pathlen, Record);
		}
		if (pathtable[i][1][1] == 'B' && pathtable[i - 1][1][1] == 'B')
			continue;
		if (pathtable[i][1][1] == 'Z')
			correctZR(pathtable, i, pathlen);
		if (pathtable[i][1][1] == 'R' + 1)
			correctZR(pathtable, i, pathlen);
	}

	for (int i = 0; i < pathlen; i++)
	{
		fout << i << " ";
		for (int j = 0; j < 4; j++)
		{
			fout << pathtable[i][j][0] << " ";
			if (pathtable[i][j][1] == 'R' + 1)
				fout << "R1 ";
			else if (pathtable[i][j][1] == 'R' + 2)
				fout << "R2 ";
			else if (pathtable[i][j][1] == 'R' + 3)
				fout << "R3 ";
			else if (pathtable[i][j][1] != 0)
				fout << char(pathtable[i][j][1]) << " ";
			else
				fout << pathtable[i][j][1] << " ";
			for (int k = 2; k < 4; k++)
				fout << pathtable[i][j][k] << " ";
		}
		fout << "\n";
	}
}

int verify(int*** pathtable, int pathlen, int** A)
{
	for (int i = 0; i < pathlen; i++)
	{
		int point = pathtable[i][1][0];
		if ((point != 10000) &&( ((i < pathlen-1)&&(pathtable[i][1][0] != pathtable[i+1][1][0]))||(i== pathlen - 1)))
		{
			for (int j = 0; j < 8; j++)
			{
				int get = 0;
				if (A[point][j] != 0)//search index table for variable number
				{
					for (int s = 0; s <= i; s++)
					{
						if (pathtable[s][0][0] == A[point][j])//search path table for necessary path procedure
						{
							get = 1;
							break;
						}
					}
					if (get == 0)
					{
						cout << i << " "<< pathtable[i][0][0] << " " << pathtable[i][0][1] << " " << pathtable[i][0][2] << " " << pathtable[i][0][3] <<"\n";
						return 1;
					}

				}
			}
		}
		
	}
	return 0;
}

int main()
{
	int T = 5;
	int E = 120 * T + 16;
	int V = 112 * (T + 1);


	int** Equation = (int**)calloc(sizeof(int*), E);//equation number 
	if (Equation != NULL)
		for (int i = 0; i < E; i++)
		{
			Equation[i] = (int*)calloc(sizeof(int), 3);
			if (Equation[i] == NULL)
				exit(0);
		}

	int** A = (int**)calloc(sizeof(int*), E); //Build initial index table
	if (A != NULL)
		for (int i = 0; i < E; i++)
		{
			A[i] = (int*)calloc(sizeof(int), 8);
			if (A[i] == NULL)
				exit(0);
		}
	BuildInitialIndexTable(A, T, Equation);
	
	int pathlen = 431;
	int*** pathtable = (int***)calloc(sizeof(int**), pathlen);
	if (pathtable != NULL)
	{
		for (int i = 0; i < pathlen; i++)
		{
			pathtable[i] = (int**)calloc(sizeof(int*), 4);
			if (pathtable[i] != NULL)
				for (int j = 0; j < 4; j++)
				{
					pathtable[i][j] = (int*)calloc(sizeof(int), 4);
					if (pathtable[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}
	readpath(pathtable, pathlen);//read the final path
	correctpath(pathtable, pathlen);

	int right = verify(pathtable, pathlen, A);
	if (right == 0)
	{
		cout << "Guessing path is right";
		/*for (int i = 0; i < pathlen; i++)
		{
			fout << i +1<< "&$";

			if (pathtable[i][0][1] == 'R' + 1)
				fout << "R1_{";
			else if (pathtable[i][0][1] == 'R' + 2)
				fout << "R2_{";
			else if (pathtable[i][0][1] == 'R' + 3)
				fout << "R3_{";
			else
				fout << char(pathtable[i][0][1]) << "_{";

			fout << pathtable[i][0][2] << "}^{" << pathtable[i][0][3] << "}$&";

			if (pathtable[i][1][1] == 'G')
				fout << "Guess&";
			else if (pathtable[i][1][1] == 'R' + 1)
				fout << "Equation \\ref\{eqR1c\}, $i=" << pathtable[i][1][2] << "$, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 2) && (pathtable[i][1][2] == 3))
				fout << "Equation \\ref\{eqR23\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 2) && (pathtable[i][1][2] == 2))
				fout << "Equation \\ref\{eqR22\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 2) && (pathtable[i][1][2] == 1))
				fout << "Equation \\ref\{eqR21\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 2) && (pathtable[i][1][2] == 0))
				fout << "Equation \\ref\{eqR20\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 3) && (pathtable[i][1][2] == 3))
				fout << "Equation \\ref\{eqR33\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 3) && (pathtable[i][1][2] == 2))
				fout << "Equation \\ref\{eqR32\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 3) && (pathtable[i][1][2] == 1))
				fout << "Equation \\ref\{eqR31\}, $t=" << pathtable[i][1][3] << "$&";
			else if ((pathtable[i][1][1] == 'R' + 3) && (pathtable[i][1][2] == 0))
				fout << "Equation \\ref\{eqR30\}, $t=" << pathtable[i][1][3] << "$&";
			else if (pathtable[i][1][1] == 'Z')
				fout << "Equation \\ref\{eqzc\}, $i=" << pathtable[i][1][2] << "$, $t=" << pathtable[i][1][3] << "$&";
			else if (pathtable[i][1][1] == 'B')
			{
				if (pathtable[i][1][2] % 2 == 1)
					fout << "Equation \\ref\{eqbacko\}, $i=" << pathtable[i][1][2] << "$, $t=" << pathtable[i][1][3] << "$&";
				if (pathtable[i][1][2] % 2 == 0)
					fout << "Equation \\ref\{eqbackd\}, $i=" << pathtable[i][1][2] << "$, $t=" << pathtable[i][1][3] << "$&";
			}
			else if (pathtable[i][1][1] == 'a')
			{
				if (pathtable[i][1][2] < 16)
					fout << "Equation \\ref\{eqas\}, $i=" << pathtable[i][1][2] << "$, $t=" << pathtable[i][1][3] << "$&";
				else if (pathtable[i][1][2] % 2 == 1)
					fout << "Equation \\ref\{eqao\}, $i=" << pathtable[i][1][2] - 16 << "$, $t=" << pathtable[i][1][3] << "$&";
				else if (pathtable[i][1][2] % 2 == 0)
					fout << "Equation \\ref\{eqad\}, $i=" << pathtable[i][1][2] - 16 << "$, $t=" << pathtable[i][1][3] << "$&";
			}
			else if (pathtable[i][1][1] == 'b')
			{
				if (pathtable[i][1][2] < 16)
					fout << "Equation \\ref\{eqbs\}, $i=" << pathtable[i][1][2] << "$, $t=" << pathtable[i][1][3] << "$&";
				else if (pathtable[i][1][2] % 2 == 1)
					fout << "Equation \\ref\{eqbo\}, $i=" << pathtable[i][1][2] - 16 << "$, $t=" << pathtable[i][1][3] << "$&";
				else if (pathtable[i][1][2] % 2 == 0)
					fout << "Equation \\ref\{eqbd\}, $i=" << pathtable[i][1][2] - 16 << "$, $t=" << pathtable[i][1][3] << "$&";
			}
			else
				fout << pathtable[i][1][1] << "!!";

			if (pathtable[i][2][1] == 0)
				fout << "&";
			else if (pathtable[i][2][1] == 'R' + 1)
				fout << "$cR1_{"<< pathtable[i][2][2] + 1<<"}^{"<< pathtable[i][2][3]<<"}$&";
			else if (pathtable[i][2][1] == 'Z')
				fout << "$cz_{" << pathtable[i][2][2] + 1 << "}^{" << pathtable[i][2][3] << "}$&";
			else if (pathtable[i][2][1] == 'a')
				fout << "$a_{" << pathtable[i][2][2] << ",7}^{" << pathtable[i][2][3] << "}$&";
			else if (pathtable[i][2][1] == 'b')
				fout << "$b_{" << pathtable[i][2][2] << ",7}^{" << pathtable[i][2][3] << "}$&";

			if (pathtable[i][3][1] == 0)
				fout << "&";
			else if (pathtable[i][3][1] == 'R' + 1)
				fout << "$cR1_{" << pathtable[i][3][2] + 1 << "}^{" << pathtable[i][3][3] << "}$&";
			else if (pathtable[i][3][1] == 'Z')
				fout << "$cz_{" << pathtable[i][3][2] + 1 << "}^{" << pathtable[i][3][3] << "}$&";
			else if (pathtable[i][3][1] == 'a')
				fout << "$a_{" << pathtable[i][3][2]<< ",7}^{" << pathtable[i][3][3] << "}$&";
			else if (pathtable[i][3][1] == 'b')
				fout << "$b_{" << pathtable[i][3][2] << ",7}^{" << pathtable[i][3][3] << "}$&";

			fout << "\n";
		}*/  //fout the path in the form of latex
	}	
	else
		cout << "Guessing path is wrong";



	return 0;
}