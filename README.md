 1. run snowvinitial.cpp for initial path, output in file "path1", and output the number of basis and paths in the screen.

   Functions of VariableTable, BuildInitialIndexTable, KnowledgePropagation, SetInitialGuessSet, and the variables in the main function, corresponding to the index table and knowledge propagation rules, need to be set for other ciphers.
   
2. run snowviRC.cpp for corrected path, output in file "path2".
 
   The input file is the initial path in file "path1".
   Functions of correctab, correctZR, correctpath corresponding to the modified rules, need to be set for other ciphers.
   
3. run snowvicorrect.cpp to verify the final path.
	
	The input file is the final path by some fine adjustments in file "pathco". The output file is for comparing.
	
	Whether the final path is right will output int the screen.

