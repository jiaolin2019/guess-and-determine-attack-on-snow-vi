#include<iostream>
#include <math.h>
#include<fstream>
#include<conio.h>
#include<stdio.h>
#include<malloc.h>
#include <time.h>
#include <vector>
using namespace std;
ifstream fin("path1.txt");
ofstream fout("path2.txt");

void readpath(int*** table, int len)
{
	for (int i = 0; i < len; i++)
		for (int j = 0; j < 2; j++)
			for (int k = 0; k < 4; k++)
				fin >> table[i][j][k];
	fin.close();
	/*for (int i = 0; i < len; i++)
	{
		fout << i << " ";
		for (int j = 0; j < 2; j++)
		{
			fout << table[i][j][0] << " ";
			if (table[i][j][1] == 'R' + 1)
				fout << "R1 ";
			else if (table[i][j][1] == 'R' + 2)
				fout << "R2 ";
			else if (table[i][j][1] == 'R' + 3)
				fout << "R3 ";
			else
				fout << char(table[i][j][1]) << " ";
			for (int k = 2; k < 4; k++)
				fout << table[i][j][k] << " ";
		}
		fout << "\n";
	}*/
}
void VariableTable(int** Variable, int T, int V)
{
	for (int t = 0; t <= T; t++)
	{
		for (int i = 1; i <= 32; i++)
		{
			Variable[i + 112 * t][0] = 'a';
			Variable[i + 112 * t][1] = i - 1;
			Variable[i + 112 * t][2] = t;
		}
		for (int i = 1; i <= 32; i++)
		{
			Variable[i + 112 * t + 32][0] = 'b';
			Variable[i + 112 * t + 32][1] = i - 1;
			Variable[i + 112 * t + 32][2] = t;
		}
		for (int i = 1; i <= 16; i++)
		{
			Variable[i + 112 * t + 64][0] = 'R' + 1;
			Variable[i + 112 * t + 64][1] = i - 1;
			Variable[i + 112 * t + 64][2] = t;
		}
		for (int i = 1; i <= 16; i++)
		{
			Variable[i + 112 * t + 80][0] = 'R' + 2;
			Variable[i + 112 * t + 80][1] = i - 1;
			Variable[i + 112 * t + 80][2] = t;
		}
		for (int i = 1; i <= 16; i++)
		{
			Variable[i + 112 * t + 96][0] = 'R' + 3;
			Variable[i + 112 * t + 96][1] = i - 1;
			Variable[i + 112 * t + 96][2] = t;
		}
	}
	/*for (int i = 1; i <= V; i++)
	{
		fout << i << " ";
		if (Variable[i][0] == 'R' + 1)
			fout << "R1 ";
		else if (Variable[i][0] == 'R' + 2)
			fout << "R2 ";
		else if (Variable[i][0] == 'R' + 3)
			fout << "R3 ";
		else
			fout << char(Variable[i][0]) << " ";
		for (int k = 1; k < 3; k++)
			fout << Variable[i][k] << " ";
		fout << "\n";
	}*/
}
int BuildInitialIndexTable(int** A, int T, int** Equation)
{
	int count = 0;
	for (int j = 0; j < 16; j++)//E:Z_15-j
		for (int t = 0; t <= T; t++)
		{
			A[count][0] = 80 - j + 112 * t;//R1
			A[count][1] = 64 - j + 112 * t;//b
			A[count][2] = 96 - j + 112 * t;//R2
			Equation[count][0] = 'Z';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:Z " << 15 - j << " " << t << "\n";
			count++;
		}

	for (int j = 0; j < 4; j++)//E:R1_15 - j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - 4 * j + 112 * (t + 1);
			A[count][1] = 96 - j + 112 * t;
			A[count][2] = 112 - j + 112 * t;
			A[count][3] = 32 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j-4
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - (4 * j + 1) + 112 * (t + 1);
			A[count][1] = 96 - 4 - j + 112 * t;
			A[count][2] = 112 - 4 - j + 112 * t;
			A[count][3] = 32 - 4 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 4;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j - 4 << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j - 8
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - (4 * j + 2) + 112 * (t + 1);
			A[count][1] = 96 - 8 - j + 112 * t;
			A[count][2] = 112 - 8 - j + 112 * t;
			A[count][3] = 32 - 8 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 8;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j - 8 << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j - 12
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 80 - (4 * j + 3) + 112 * (t + 1);
			A[count][1] = 96 - 12 - j + 112 * t;
			A[count][2] = 112 - 12 - j + 112 * t;
			A[count][3] = 32 - 12 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 12;
			Equation[count][2] = t;
			//fout << "E:R1 " << 15 - j - 12 << " " << t << "\n";
			count++;
		}

	for (int j = 0; j < 16; j++)//E:a_32-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 32 - j + 112 * (t + 1);
			A[count][1] = 48 - j + 112 * t;
			A[count][2] = 16 - j + 112 * t;
			A[count][3] = 30 - j + 112 * t;
			Equation[count][0] = 'a';
			Equation[count][1] = 31 - j;
			Equation[count][2] = t;
			//fout << "E:a " << 31 - j << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 16; j++)//E:a_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 16 - j + 112 * (t + 1);
			A[count][1] = 32 - j + 112 * t;
			Equation[count][0] = 'a';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:a " << 15 - j << " " << t << "\n";
			count++;
		}

	for (int j = 0; j < 16; j++)//E:b_32-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 64 - j + 112 * (t + 1);
			A[count][1] = 16 - j + 112 * t;
			A[count][2] = 48 - j + 112 * t;
			A[count][3] = 64 - j + 112 * t;
			Equation[count][0] = 'b';
			Equation[count][1] = 31 - j;
			Equation[count][2] = t;
			//fout << "E:b " << 31 - j << " " << t << "\n";
			count++;
		}
	for (int j = 0; j < 16; j++)//E:b_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 48 - j + 112 * (t + 1);
			A[count][1] = 64 - j + 112 * t;
			Equation[count][0] = 'b';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:b " << 15 - j << " " << t << "\n";
			count++;
		}

	for (int t = 0; t < T; t++)//E:R2_3
	{
		A[count][0] = 93 + 112 * (t + 1);
		A[count][1] = 94 + 112 * (t + 1);
		A[count][2] = 95 + 112 * (t + 1);
		A[count][3] = 96 + 112 * (t + 1);
		A[count][4] = 77 + 112 * t;
		A[count][5] = 66 + 112 * t;
		A[count][6] = 71 + 112 * t;
		A[count][7] = 76 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 3;
		Equation[count][2] = t;
		//fout << "E:R2 3 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_2
	{
		A[count][0] = 89 + 112 * (t + 1);
		A[count][1] = 90 + 112 * (t + 1);
		A[count][2] = 91 + 112 * (t + 1);
		A[count][3] = 92 + 112 * (t + 1);
		A[count][4] = 73 + 112 * t;
		A[count][5] = 78 + 112 * t;
		A[count][6] = 67 + 112 * t;
		A[count][7] = 72 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 2;
		Equation[count][2] = t;
		//fout << "E:R2 2 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_1
	{
		A[count][0] = 85 + 112 * (t + 1);
		A[count][1] = 86 + 112 * (t + 1);
		A[count][2] = 87 + 112 * (t + 1);
		A[count][3] = 88 + 112 * (t + 1);
		A[count][4] = 69 + 112 * t;
		A[count][5] = 74 + 112 * t;
		A[count][6] = 79 + 112 * t;
		A[count][7] = 68 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 1;
		Equation[count][2] = t;
		//fout << "E:R2 1 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_0
	{
		A[count][0] = 81 + 112 * (t + 1);
		A[count][1] = 82 + 112 * (t + 1);
		A[count][2] = 83 + 112 * (t + 1);
		A[count][3] = 84 + 112 * (t + 1);
		A[count][4] = 65 + 112 * t;
		A[count][5] = 70 + 112 * t;
		A[count][6] = 75 + 112 * t;
		A[count][7] = 80 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 0;
		Equation[count][2] = t;
		//fout << "E:R2 0 " << t << "\n";
		count++;
	}

	for (int t = 0; t < T; t++)//E:R3_3
	{
		A[count][0] = 109 + 112 * (t + 1);
		A[count][1] = 110 + 112 * (t + 1);
		A[count][2] = 111 + 112 * (t + 1);
		A[count][3] = 112 + 112 * (t + 1);
		A[count][4] = 93 + 112 * t;
		A[count][5] = 82 + 112 * t;
		A[count][6] = 87 + 112 * t;
		A[count][7] = 92 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 3;
		Equation[count][2] = t;
		//fout << "E:R3 3 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_2
	{
		A[count][0] = 105 + 112 * (t + 1);
		A[count][1] = 106 + 112 * (t + 1);
		A[count][2] = 107 + 112 * (t + 1);
		A[count][3] = 108 + 112 * (t + 1);
		A[count][4] = 89 + 112 * t;
		A[count][5] = 94 + 112 * t;
		A[count][6] = 83 + 112 * t;
		A[count][7] = 88 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 2;
		Equation[count][2] = t;
		//fout << "E:R3 2 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_1
	{
		A[count][0] = 101 + 112 * (t + 1);
		A[count][1] = 102 + 112 * (t + 1);
		A[count][2] = 103 + 112 * (t + 1);
		A[count][3] = 104 + 112 * (t + 1);
		A[count][4] = 85 + 112 * t;
		A[count][5] = 90 + 112 * t;
		A[count][6] = 95 + 112 * t;
		A[count][7] = 84 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 1;
		Equation[count][2] = t;
		//fout << "E:R3 1 " << t << "\n";
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_0
	{
		A[count][0] = 97 + 112 * (t + 1);
		A[count][1] = 98 + 112 * (t + 1);
		A[count][2] = 99 + 112 * (t + 1);
		A[count][3] = 100 + 112 * (t + 1);
		A[count][4] = 81 + 112 * t;
		A[count][5] = 86 + 112 * t;
		A[count][6] = 91 + 112 * t;
		A[count][7] = 96 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 0;
		Equation[count][2] = t;
		//fout << "E:R3 0 " << t << "\n";
		count++;
	}

	for (int j = 0; j < 16; j++)//back_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 64 - j + 112 * (t + 1);
			A[count][1] = 32 - j + 112 * (t + 1);
			A[count][2] = 30 - j + 112 * t;
			A[count][3] = 64 - j + 112 * t;
			A[count][4] = 48 - j + 112 * t;
			A[count][5] = 16 - j + 112 * t;
			Equation[count][0] = 'B';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			//fout << "E:back " << 15 - j << " " << t << "\n";
			count++;
		}

	/*for (int i = 0; i < count; i++)
	{
		fout << i << " ";
		for (int j = 0; j < 8; j++)
			fout << A[i][j] << " ";
		fout << "\n";
	}*/

	return count;
}

int pathremove(int*** table, int** A, int len, int t, int* B)
{
	int p = 0;
	for (int i = len - 1; i >= 0; i--)
	{
		if (table[i][0][3] == t)//keep t time proceduce
		{
			int repetition = 0;
			for (int u = 0; u < p; u++)
				if (B[u] == i)
					repetition = 1;
			if (repetition == 0)
			{
				B[p] = i;
				p++;
			}

			for (int k = 0; k < p; k++)//loop in B[]
			{
				for (int j = 0; j < 8; j++)
				{
					int point = table[B[k]][1][0];//search path table for equation number
					if ((point != 10000) && (A[point][j] != 0))//search index table for variable number
					{
						for (int s = len - 1; s >= 0; s--)
						{
							if (table[s][0][0] == A[point][j])//search path table for necessary path procedure
							{
								int repetition = 0;
								for (int u = 0; u < p; u++)
									if (B[u] == s)
										repetition = 1;
								if (repetition == 0)
								{
									B[p] = s;
									p++;
								}
							}
						}
					}
				}
			}
		}
	}


	/*for (int i = 0; i < p; i++)
	{
		fout << B[i] << " ";
	}
		fout << "\n";
	*/

	int i, j;
	for (i = 0; i < p; i++)//rank remained procedures
	{
		for (j = 0; j < p - i - 1; j++)
		{
			if (B[j] > B[j + 1])
			{
				int temp;
				temp = B[j];
				B[j] = B[j + 1];
				B[j + 1] = temp;
			}
		}
	}
	/*for (int i = 0; i < p; i++)
	{
		fout << B[i] << " ";
	}
	fout << "\n";*/

	return p;
}

void correctab(int*** pathtable, int i, int eq, int pm, int pathlen, int** Record)//correct for eqaution a and b, component 15~31
{
	int flag;
	if ((pathtable[i][0][1] == eq) && (pathtable[i][0][2] == pathtable[i][1][2] - 16)&& (pathtable[i][0][3] == pathtable[i][1][3]))
	{//determine eq_i^t
		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if (((pathtable[j][0][1] == pathtable[i][0][1]) && (pathtable[j][0][2] == pathtable[i][0][2]) && (pathtable[j][0][3] == pathtable[i][0][3])) || ((pathtable[j][2][1] == pathtable[i][0][1]) && (pathtable[j][2][2] == pathtable[i][0][2]) && (pathtable[j][2][3] == pathtable[i][0][3])) || ((Record[j][0] == pathtable[i][0][1]) && (Record[j][1] == pathtable[i][0][2]) && (Record[j][2] == pathtable[i][0][3])))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)//eq_i,7^t is unknown
		{
			pathtable[i][2][0]++;
			pathtable[i][2][1] = pathtable[i][0][1];
			pathtable[i][2][2] = pathtable[i][0][2];
			pathtable[i][2][3] = pathtable[i][0][3];
		}
		if ((pathtable[i][3][1] == pathtable[i][0][1]) && (pathtable[i][3][2] == pathtable[i][0][2]) && (pathtable[i][3][3] == pathtable[i][0][3]))
		{//cannot use for reduce eq_i,7^t
			pathtable[i][3][0]++;
			pathtable[i][3][1] = 0;
			pathtable[i][3][2] = 0;
			pathtable[i][3][3] = 0;
		}

		Record[i][0] = pathtable[i][0][1];//determine eq_i-1,7^t
		Record[i][1] = pathtable[i][0][2] + pm;
		Record[i][2] = pathtable[i][0][3];

		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if (((pathtable[j][0][1] == pathtable[i][0][1]) && (pathtable[j][0][2] == pathtable[i][0][2] + pm) && (pathtable[j][0][3] == pathtable[i][0][3])) || ((pathtable[j][2][1] == pathtable[i][0][1]) && (pathtable[j][2][2] == pathtable[i][0][2] + pm) && (pathtable[j][2][3] == pathtable[i][0][3])))
			{//reduce eq_i-1,7^t for proceduce guessed before
				flag = 1;
				pathtable[i][3][0]--;
				pathtable[i][3][1] = pathtable[i][0][1];
				pathtable[i][3][2] = pathtable[i][0][2] + pm;
				pathtable[i][3][3] = pathtable[i][0][3];			
			}
		}
		if (flag == 0)
		{
			for (int j = i + 1; j < pathlen; j++)
			{//reduce eq_i-1,7^t for proceduce guessed later
				if ((pathtable[j][0][1] == pathtable[i][0][1]) && (pathtable[j][0][2] == pathtable[i][0][2] + pm) && (pathtable[j][0][3] == pathtable[i][0][3]))
				{
					pathtable[j][3][0]--;
					pathtable[j][3][1] = pathtable[i][0][1];
					pathtable[j][3][2] = pathtable[i][0][2] + pm;
					pathtable[j][3][3] = pathtable[i][0][3]; 
				}
			}
		}
		
	}
	else
	{//determine other variable
		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if (((pathtable[j][0][1] == eq) && (pathtable[j][0][2] == pathtable[i][1][2] - 16 + pm) && (pathtable[j][0][3] == pathtable[i][1][3])) || ((pathtable[j][2][1] == eq) && (pathtable[j][2][2] == pathtable[i][1][2] - 16 + pm) && (pathtable[j][2][3] == pathtable[i][1][3])) || ((Record[j][0] == eq) && (Record[j][1] == pathtable[i][0][2] - 16 + pm) && (Record[j][2] == pathtable[i][1][3])))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)//eq_i-1,7^t is unknown
		{
			pathtable[i][2][0]++;
			pathtable[i][2][1] = eq;
			pathtable[i][2][2] = pathtable[i][1][2] - 16 + pm;
			pathtable[i][2][3] = pathtable[i][1][3];
			for (int j = i + 1; j < pathlen; j++)
			{//reduce eq_i-1,7^t for proceduce guessed later
				if ((pathtable[j][0][1] == eq) && (pathtable[j][0][2] == pathtable[i][1][2] - 16 + pm) && (pathtable[j][0][3] == pathtable[i][1][3]))
				{
					pathtable[j][3][0]--;
					pathtable[j][3][1] = eq;
					pathtable[j][3][2] = pathtable[i][1][2] - 16 + pm;
					pathtable[j][3][3] = pathtable[i][1][3];				
				}
			}
		}

	}
}

void correctZR(int*** pathtable, int i, int pathlen)
{
	int flag;
	if ((pathtable[i][1][2] != 0) && (pathtable[i][1][2] != 4) && (pathtable[i][1][2] != 8) && (pathtable[i][1][2] != 12))
	{
		flag = 0;
		for (int j = 0; j < i; j++)
		{
			if ((pathtable[j][1][1] == pathtable[i][1][1]) && (pathtable[j][1][2] == pathtable[i][1][2] - 1) && (pathtable[j][1][3] == pathtable[i][1][3]))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)//carry bit is unknown
		{
			pathtable[i][2][0]++;
			pathtable[i][2][1] = pathtable[i][1][1];
			pathtable[i][2][2] = pathtable[i][1][2] - 1;
			pathtable[i][2][3] = pathtable[i][1][3];
			for (int k = i + 1; k < pathlen; k++)
			{//reduce carry bit later
				if ((pathtable[k][1][1] == pathtable[i][1][1]) && (pathtable[k][1][2] == pathtable[i][1][2] - 1) && (pathtable[k][1][3] == pathtable[i][1][3]))
				{
					pathtable[k][3][0]--;
					pathtable[k][3][1] = pathtable[i][1][1];
					pathtable[k][3][2] = pathtable[i][1][2] - 1;
					pathtable[k][3][3] = pathtable[i][1][3];
				}
			}
		}
	}

	
}

void correctpath(int*** pathtable, int*** table, int* B, int pathlen)
{
	int count = 0;
	for (int i = 0; i < pathlen; i++)//keep the necessary path procedues
	{
		for (int j = 0; j < 2; j++)
			for (int k = 0; k < 4; k++)
				pathtable[count][j][k] = table[B[i]][j][k];
		count++;
	}
	/*if (count == pathlen)
		cout << "right\n";*/

	int** Record = (int**)calloc(sizeof(int*), pathlen);//record the determine bits not in path table
	if (Record != NULL)
		for (int i = 0; i < pathlen; i++)
		{
			Record[i] = (int*)calloc(sizeof(int), 3);
			if (Record[i] == NULL)
				exit(0);
		}

	
	for (int i = 0; i < pathlen; i++)
	{
		if (pathtable[i][1][1] == 'a')
		{
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 1))
				correctab(pathtable, i, 'a', -1, pathlen, Record);
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 0))
				correctab(pathtable, i, 'a',1, pathlen, Record);
		}
		if (pathtable[i][1][1] == 'b')
		{
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 1))
				correctab(pathtable, i, 'b', -1, pathlen, Record);
			if ((pathtable[i][1][2] >= 16) && (pathtable[i][1][2] % 2 == 0))
				correctab(pathtable, i, 'b', 1, pathlen, Record);
		}
		if (pathtable[i][1][1] == 'Z')
			correctZR(pathtable, i, pathlen);
		if (pathtable[i][1][1] == 'R' + 1)
			correctZR(pathtable, i, pathlen);
	}

	/*for (int i = 0; i < pathlen; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			for(int k=0;k<4;k++)
				fout << pathtable[i][j][k] << " ";
		}
		fout << "\n";
	}*/
	for (int i = 0; i < pathlen; i++)
	{
		fout << i << " ";
		for (int j = 0; j < 5; j++)
		{
			fout << pathtable[i][j][0] << " ";
			if (pathtable[i][j][1] == 'R' + 1)
				fout << "R1 ";
			else if (pathtable[i][j][1] == 'R' + 2)
				fout << "R2 ";
			else if (pathtable[i][j][1] == 'R' + 3)
				fout << "R3 ";
			else if (pathtable[i][j][1] != 0)
				fout << char(pathtable[i][j][1]) << " ";
			else
				fout << pathtable[i][j][1] << " ";
			for (int k = 2; k < 4; k++)
				fout << pathtable[i][j][k] << " ";
		}
		fout << "\n";
	}
}

int main()
{
	int T = 5;
	int E = 120 * T + 16;
	int V = 112 * (T + 1);

	int len = 672;
	int t = 2;

	int** Equation = (int**)calloc(sizeof(int*), E);//equation number 
	if (Equation != NULL)
		for (int i = 0; i < E; i++)
		{
			Equation[i] = (int*)calloc(sizeof(int), 3);
			if (Equation[i] == NULL)
				exit(0);
		}

	int** Variable = (int**)calloc(sizeof(int*), V + 1);//variable number 
	if (Variable != NULL)
		for (int i = 0; i < V + 1; i++)
		{
			Variable[i] = (int*)calloc(sizeof(int), 3);
			if (Variable[i] == NULL)
				exit(0);
		}
	VariableTable(Variable, T, V);

	int** A = (int**)calloc(sizeof(int*), E); //Build initial index table
	if (A != NULL)
		for (int i = 0; i < E; i++)
		{
			A[i] = (int*)calloc(sizeof(int), 8);
			if (A[i] == NULL)
				exit(0);
		}
	BuildInitialIndexTable(A, T, Equation);

	int*** table = (int***)calloc(sizeof(int**), len);//original path
	if (table != NULL)
	{
		for (int i = 0; i < len; i++)
		{
			table[i] = (int**)calloc(sizeof(int*), 2);
			if (table[i] != NULL)
				for (int j = 0; j < 2; j++)
				{
					table[i][j] = (int*)calloc(sizeof(int), 4);
					if (table[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}
	readpath(table, len);// read the path table

	int* B = (int*)calloc(sizeof(int), len);//remove path procedure
	int pathlen = pathremove(table, A, len, t, B);

	int*** pathtable = (int***)calloc(sizeof(int**), pathlen);//correct path
	if (pathtable != NULL)
	{
		for (int i = 0; i < pathlen; i++)
		{
			pathtable[i] = (int**)calloc(sizeof(int*), 4);
			if (pathtable[i] != NULL)
				for (int j = 0; j < 5; j++)
				{
					pathtable[i][j] = (int*)calloc(sizeof(int), 4);
					if (pathtable[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}
	correctpath(pathtable, table, B, pathlen);

	return 0;
}