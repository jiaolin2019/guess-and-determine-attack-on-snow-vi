#include<iostream>
#include <math.h>
#include<fstream>
#include<conio.h>
#include<stdio.h>
#include<malloc.h>
#include <time.h>
#include <vector>
using namespace std;
ofstream fout("path1.txt");

//a_0~31:1~32 + 112t; b_0~31:33~64 + 112t;R1_0~15:65~80 + 112t; R2_0~15:81~96 + 112t; R3_0~15:97~112 + 112t;
void VariableTable(int** Variable, int T)
{
	for (int t = 0; t <= T; t++)
	{
		for (int i = 1; i <= 32; i++)
		{
			Variable[i + 112 * t][0] = 'a';
			Variable[i + 112 * t][1] = i-1;
			Variable[i + 112 * t][2] = t;
		}
		for (int i = 1; i <= 32; i++)
		{
			Variable[i + 112 * t + 32][0] = 'b';
			Variable[i + 112 * t + 32][1] = i-1;
			Variable[i + 112 * t + 32][2] = t;
		}
		for (int i = 1; i <= 16; i++)
		{
			Variable[i + 112 * t + 64][0] = 'R' + 1;
			Variable[i + 112 * t + 64][1] = i-1;
			Variable[i + 112 * t + 64][2] = t;
		}
		for (int i = 1; i <= 16; i++)
		{
			Variable[i + 112 * t + 80][0] = 'R' + 2;
			Variable[i + 112 * t + 80][1] = i-1;
			Variable[i + 112 * t + 80][2] = t;
		}
		for (int i = 1; i <= 16; i++)
		{
			Variable[i + 112 * t + 96][0] = 'R' + 3;
			Variable[i + 112 * t + 96][1] = i-1;
			Variable[i + 112 * t + 96][2] = t;
		}
	}
}
int BuildInitialIndexTable(int** A, int T, int **Equation)
{
	int count = 0;
	for (int j = 0; j < 16; j++)//E:Z_15-j
		for (int t = 0; t <= T; t++)
		{
			A[count][0] = 3;
			A[count][1] = 80 - j + 112 * t;//R1
			A[count][2] = 64 - j + 112 * t;//b
			A[count][3] = 96 - j + 112 * t;//R2
			Equation[count][0] = 'Z';
			Equation[count][1] = 15-j;
			Equation[count][2] = t;
			count++;
		}

	for (int j = 0; j < 4; j++)//E:R1_15 - j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 4;
			A[count][1] = 80 - 4 * j + 112 * (t + 1);
			A[count][2] = 96 - j + 112 * t;
			A[count][3] = 112 - j + 112 * t;
			A[count][4] = 32 - j + 112 * t;
			Equation[count][0] = 'R'+1;
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j-4
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 4;
			A[count][1] = 80 - (4 * j + 1) + 112 * (t + 1);
			A[count][2] = 96 - 4 - j + 112 * t;
			A[count][3] = 112 - 4 - j + 112 * t;
			A[count][4] = 32 - 4 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 4;
			Equation[count][2] = t;
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j - 8
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 4;
			A[count][1] = 80 - (4 * j + 2) + 112 * (t + 1);
			A[count][2] = 96 - 8 - j + 112 * t;
			A[count][3] = 112 - 8 - j + 112 * t;
			A[count][4] = 32 - 8 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 8;
			Equation[count][2] = t;
			count++;
		}
	for (int j = 0; j < 4; j++)//E:R1_15 - j - 12
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 4;
			A[count][1] = 80 - (4 * j + 3) + 112 * (t + 1);
			A[count][2] = 96 - 12 - j + 112 * t;
			A[count][3] = 112 - 12 - j + 112 * t;
			A[count][4] = 32 - 12 - j + 112 * t;
			Equation[count][0] = 'R' + 1;
			Equation[count][1] = 15 - j - 12;
			Equation[count][2] = t;
			count++;
		}

	for (int j = 0; j < 16; j++)//E:a_32-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 4;
			A[count][1] = 32 - j + 112 * (t + 1);
			A[count][2] = 48 - j + 112 * t;
			A[count][3] = 16 - j + 112 * t;
			A[count][4] = 30 - j + 112 * t;
			Equation[count][0] = 'a';
			Equation[count][1] = 31 - j;
			Equation[count][2] = t;
			count++;
		}
	for (int j = 0; j < 16; j++)//E:a_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 2;
			A[count][1] = 16 - j + 112 * (t + 1);
			A[count][2] = 32 - j + 112 * t;
			Equation[count][0] = 'a';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			count++;
		}

	for (int j = 0; j < 16; j++)//E:b_32-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 4;
			A[count][1] = 64 - j + 112 * (t + 1);
			A[count][2] = 16 - j + 112 * t;
			A[count][3] = 48 - j + 112 * t;
			A[count][4] = 64 - j + 112 * t;
			Equation[count][0] = 'b';
			Equation[count][1] = 31 - j;
			Equation[count][2] = t;
			count++;
		}
	for (int j = 0; j < 16; j++)//E:b_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 2;
			A[count][1] = 48 - j + 112 * (t + 1);
			A[count][2] = 64 - j + 112 * t;
			Equation[count][0] = 'b';
			Equation[count][1] = 15 - j;
			Equation[count][2] = t;
			count++;
		}

	for (int t = 0; t < T; t++)//E:R2_3
	{
		A[count][0] = 8;
		A[count][1] = 93 + 112 * (t + 1);
		A[count][2] = 94 + 112 * (t + 1);
		A[count][3] = 95 + 112 * (t + 1);
		A[count][4] = 96 + 112 * (t + 1);
		A[count][5] = 77 + 112 * t;
		A[count][6] = 66 + 112 * t;
		A[count][7] = 71 + 112 * t;
		A[count][8] = 76 + 112 * t;
		Equation[count][0] = 'R'+2;
		Equation[count][1] = 3;
		Equation[count][2] = t;
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_2
	{
		A[count][0] = 8;
		A[count][1] = 89 + 112 * (t + 1);
		A[count][2] = 90 + 112 * (t + 1);
		A[count][3] = 91 + 112 * (t + 1);
		A[count][4] = 92 + 112 * (t + 1);
		A[count][5] = 73 + 112 * t;
		A[count][6] = 78 + 112 * t;
		A[count][7] = 67 + 112 * t;
		A[count][8] = 72 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 2;
		Equation[count][2] = t;
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_1
	{
		A[count][0] = 8;
		A[count][1] = 85 + 112 * (t + 1);
		A[count][2] = 86 + 112 * (t + 1);
		A[count][3] = 87 + 112 * (t + 1);
		A[count][4] = 88 + 112 * (t + 1);
		A[count][5] = 69 + 112 * t;
		A[count][6] = 74 + 112 * t;
		A[count][7] = 79 + 112 * t;
		A[count][8] = 68 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 1;
		Equation[count][2] = t;
		count++;
	}
	for (int t = 0; t < T; t++)//E:R2_0
	{
		A[count][0] = 8;
		A[count][1] = 81 + 112 * (t + 1);
		A[count][2] = 82 + 112 * (t + 1);
		A[count][3] = 83 + 112 * (t + 1);
		A[count][4] = 84 + 112 * (t + 1);
		A[count][5] = 65 + 112 * t;
		A[count][6] = 70 + 112 * t;
		A[count][7] = 75 + 112 * t;
		A[count][8] = 80 + 112 * t;
		Equation[count][0] = 'R' + 2;
		Equation[count][1] = 0;
		Equation[count][2] = t;
		count++;
	}

	for (int t = 0; t < T; t++)//E:R3_3
	{
		A[count][0] = 8;
		A[count][1] = 109 + 112 * (t + 1);
		A[count][2] = 110 + 112 * (t + 1);
		A[count][3] = 111 + 112 * (t + 1);
		A[count][4] = 112 + 112 * (t + 1);
		A[count][5] = 93 + 112 * t;
		A[count][6] = 82 + 112 * t;
		A[count][7] = 87 + 112 * t;
		A[count][8] = 92 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 3;
		Equation[count][2] = t;
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_2
	{
		A[count][0] = 8;
		A[count][1] = 105 + 112 * (t + 1);
		A[count][2] = 106 + 112 * (t + 1);
		A[count][3] = 107 + 112 * (t + 1);
		A[count][4] = 108 + 112 * (t + 1);
		A[count][5] = 89 + 112 * t;
		A[count][6] = 94 + 112 * t;
		A[count][7] = 83 + 112 * t;
		A[count][8] = 88 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 2;
		Equation[count][2] = t;
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_1
	{
		A[count][0] = 8;
		A[count][1] = 101 + 112 * (t + 1);
		A[count][2] = 102 + 112 * (t + 1);
		A[count][3] = 103 + 112 * (t + 1);
		A[count][4] = 104 + 112 * (t + 1);
		A[count][5] = 85 + 112 * t;
		A[count][6] = 90 + 112 * t;
		A[count][7] = 95 + 112 * t;
		A[count][8] = 84 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 1;
		Equation[count][2] = t;
		count++;
	}
	for (int t = 0; t < T; t++)//E:R3_0
	{
		A[count][0] = 8;
		A[count][1] = 97 + 112 * (t + 1);
		A[count][2] = 98 + 112 * (t + 1);
		A[count][3] = 99 + 112 * (t + 1);
		A[count][4] = 100 + 112 * (t + 1);
		A[count][5] = 81 + 112 * t;
		A[count][6] = 86 + 112 * t;
		A[count][7] = 91 + 112 * t;
		A[count][8] = 96 + 112 * t;
		Equation[count][0] = 'R' + 3;
		Equation[count][1] = 0;
		Equation[count][2] = t;
		count++;
	}

	for (int j = 0; j < 16; j++)//back_16-j
		for (int t = 0; t < T; t++)
		{
			A[count][0] = 6;
			A[count][1] = 64 - j + 112 * (t + 1);
			A[count][2] = 32 - j + 112 * (t + 1);
			A[count][3] = 30 - j + 112 * t;
			A[count][4] = 64 - j + 112 * t;
			A[count][5] = 48 - j + 112 * t;
			A[count][6] = 16 - j + 112 * t;
			Equation[count][0] = 'B';
			Equation[count][1] = 15-j;
			Equation[count][2] = t;
			count++;
		}
	return count;
}

int KnowledgePropagation(int** A, int** B, int T, int M, int I, int E, int* point)
{
	int num = 0;//number of removed entries
	for (int l = 0; l < *point; l++)
	{
		for (int i = 0; i < M; i++)
		{
			for (int j = 1; j < 9; j++)
				if (A[i][j] == B[l][0])
				{
					num++;
					A[i][j] = 0;
					A[i][0]--;
				}
			if (A[i][0] == 1)
			{
				for (int j = 1; j < 9; j++)
					if (A[i][j] != 0)
					{
						int repetition = 0;
						for (int u = 0; u < *point; u++)
							if (B[u][0] == A[i][j])
								repetition = 1;
						if (repetition == 0)
						{
							B[*point][0] = A[i][j];
							B[*point][1] = i;
							(*point)++;
						} //Check whether the determined value is in B, otherwise add it to B 
						num++;
						A[i][j] = 0;
					}
				A[i][0] = 0;
			}
		}
		for (int i = M; i < I; i++)
		{
			for (int j = 1; j < 9; j++)
				if (A[i][j] == B[l][0])
				{
					num++;
					A[i][j] = 0;
					A[i][0]--;
				}
			if (A[i][0] == 4)
			{
				for (int j = 1; j < 9; j++)
					if (A[i][j] != 0)
					{
						int repetition = 0;
						for (int u = 0; u < *point; u++)
							if (B[u][0] == A[i][j])
								repetition = 1;
						if (repetition == 0)
						{
							B[*point][0] = A[i][j];
							B[*point][1] = i;
							(*point)++;
						}
						num++;
						A[i][j] = 0;
					}
				A[i][0] = 0;
			}
		}
		for (int i = I; i < E; i++)
		{
			for (int j = 1; j < 9; j++)
				if (A[i][j] == B[l][0])
				{
					num++;
					A[i][j] = 0;
					A[i][0]--;
				}
			if ((A[i][1] == 0) && (A[i][2] == 0) && (A[i][3] == 0) && (A[i][4] == 0))
			{
				for (int j = 1; j < 9; j++)
					if (A[i][j] != 0)
					{
						int repetition = 0;
						for (int u = 0; u < *point; u++)
							if (B[u][0] == A[i][j])
								repetition = 1;
						if (repetition == 0)
						{
							B[*point][0] = A[i][j];
							B[*point][1] = i;
							(*point)++;
						}
						num++;
						A[i][j] = 0;
					}
				A[i][0] = 0;
			}
		}
	}
	return num;

}

int PrintResult(int** C, int V, int T, int x, int ***table,int **Variable, int **Equation)
{
	int len;
	for (int i = 0; (i < V) && (C[i][0] != 0); i++)
	{
		len = i;
		table[i + x][0][0] = C[i][0];
		table[i + x][0][1] = Variable[C[i][0]][0];
		table[i + x][0][2] = Variable[C[i][0]][1];
		table[i + x][0][3] = Variable[C[i][0]][2];
					
		if (C[i][1] < 10000)
		{
			table[i + x][1][0] = C[i][1];
			table[i + x][1][1] = Equation[C[i][1]][0];
			table[i + x][1][2] = Equation[C[i][1]][1];
			table[i + x][1][3] = Equation[C[i][1]][2];
		}
		else
		{
			table[i + x][1][0] = C[i][1];
			table[i + x][1][1] = 'G';
		}	
	}
	return len;
}

int SetInitialGuessSet(int** C)
{
	C[0][0] = 77 + 112 * 2; C[1][0] = 66 + 112 * 2; C[2][0] = 71 + 112 * 2; C[3][0] = 76 + 112 * 2; //Set the initial guessing set
	C[4][0] = 73 + 112 * 2; C[5][0] = 78 + 112 * 2; C[6][0] = 67 + 112 * 2; C[7][0] = 72 + 112 * 2;
	C[8][0] = 61 + 112 * 2; C[9][0] = 50 + 112 * 2; C[10][0] = 55 + 112 * 2; C[11][0] = 60 + 112 * 2;
	int pp = 12;
	for (int i = 0; i < pp; i++) //Mark as a guess variable
		C[i][1] = 10000;
	return pp;
}

void HGDalgorithm(int T, int M, int I, int E, int V, int const1)
{
	int len;
	int** Equation = (int**)calloc(sizeof(int*), E); //equation number 
	if (Equation != NULL)
		for (int i = 0; i < E; i++)
		{
			Equation[i] = (int*)calloc(sizeof(int), 3);
			if (Equation[i] == NULL)
				exit(0);
		}
	int** Variable = (int**)calloc(sizeof(int*), V+1); //variable number 
	if (Variable != NULL)
		for (int i = 0; i < V + 1; i++)
		{
			Variable[i] = (int*)calloc(sizeof(int), 3);
			if (Variable[i] == NULL)
				exit(0);
		}
	VariableTable(Variable, T);

	int*** table = (int***)calloc(sizeof(int**), V);//record path
	if (table != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			table[i] = (int**)calloc(sizeof(int*), 2);
			if (table[i] != NULL)
				for (int j = 0; j < 2; j++)
				{
					table[i][j] = (int*)calloc(sizeof(int), 4);
					if (table[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}

	int** A = (int**)calloc(sizeof(int*), E); //Build initial index table
	if (A != NULL)
		for (int i = 0; i < E; i++)
		{
			A[i] = (int*)calloc(sizeof(int), const1);
			if (A[i] == NULL)
				exit(0);
		}
	if (BuildInitialIndexTable(A, T,Equation) == E)
		cout << "Initial index table is right.\n";
	else
		cout << "Initial index table is wrong.\n";


	int** C = (int**)calloc(sizeof(int*), V);//Set initial guessing set
	if (C != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			C[i] = (int*)calloc(sizeof(int), 2);
			if (C[i] == NULL)
				exit(0);
		}
	}
	int pp = SetInitialGuessSet(C);
	KnowledgePropagation(A, C, T, M, I, E, &pp);
	//fout << "Initial guessing set:\n";
	PrintResult(C, V, T, 0, table,Variable,Equation);


	int flag = 0;//Mark the basis
	int pathnum = 0;//number of basis
	int first = 0;//Mark the first basis

	int*** index = (int***)calloc(sizeof(int**), V); //The former column in the trellis diagram
	if (index != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			index[i] = (int**)calloc(sizeof(int*), E);
			if (index[i] != NULL)
				for (int j = 0; j < E; j++)
				{
					index[i][j] = (int*)calloc(sizeof(int), const1);
					if (index[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}
	for (int k = 0; k < V; k++)//Initialize with A
		for (int i = 0; i < E; i++)
			for (int j = 0; j < const1; j++)
				index[k][i][j] = A[i][j];

	int*** path = (int***)calloc(sizeof(int**), V); //The former path
	if (path != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			path[i] = (int**)calloc(sizeof(int*), V);
			if (path[i] != NULL)
				for (int j = 0; j < V; j++)
				{
					path[i][j] = (int*)calloc(sizeof(int), 2);
					if (path[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}

	int*** indexnew = (int***)calloc(sizeof(int**), V); //The next column in the trellis diagram
	if (indexnew != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			indexnew[i] = (int**)calloc(sizeof(int*), E);
			if (indexnew[i] != NULL)
				for (int j = 0; j < E; j++)
				{
					indexnew[i][j] = (int*)calloc(sizeof(int), const1);
					if (indexnew[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}

	int*** pathnew = (int***)calloc(sizeof(int**), V); //The updated path
	if (pathnew != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			pathnew[i] = (int**)calloc(sizeof(int*), V);
			if (pathnew[i] != NULL)
				for (int j = 0; j < V; j++)
				{
					pathnew[i][j] = (int*)calloc(sizeof(int), 2);
					if (pathnew[i][j] == NULL)
						exit(0);
				}
			else
				exit(0);
		}
	}

	int** B = (int**)calloc(sizeof(int*), V); //removed set
	if (B != NULL)
	{
		for (int i = 0; i < V; i++)
		{
			B[i] = (int*)calloc(sizeof(int), 2);
			if (B[i] == NULL)
				exit(0);
		}
	}
	int point;

	int guessmin = V;//Record the minimum number of guesses

	for (int t = 0; t < V; t++)//loop in the level of trellis diagram
	{
		for (int s = 1; s <= V; s++)//loop in the coming variables
		{
			int al = 0;//Check whether in the initial guessing set
			for (int x = 0; x < pp; x++)
				if (s == C[x][0])
					al = 1;
			if (al == 0)
			{
				int record = 0;//Record the output variable that removes the most entries
				for (int k = 0; k < V; k++)//loop in the output variables
				{

					for (int i = 0; i < V; i++)
						for (int j = 0; j < 2; j++)
							B[i][j] = 0;

					B[0][0] = s;
					point = 1;//Guess s-th variable based on original paths

					for (int i = 0; i < E; i++)
						for (int j = 0; j < const1; j++)
							A[i][j] = index[k][i][j];

					int num = KnowledgePropagation(A, B, T, M, I, E, &point);

					if (record < num)//Record better choice
					{
						record = num;
						for (int i = 0; i < V; i++)
							for (int j = 0; j < 2; j++)
								pathnew[s - 1][i][j] = 0;
						for (int i = 0; i < E; i++)
							for (int j = 0; j < const1; j++)
								indexnew[s - 1][i][j] = A[i][j];
						int tag = 0;
						for (int i = 0; i < V; i++)
							if (path[k][i][0] != 0)
							{
								for (int j = 0; j < 2; j++)
									pathnew[s - 1][tag][j] = path[k][i][j];
								tag++;
							}
						pathnew[s - 1][tag][0] = B[0][0];
						pathnew[s - 1][tag][1] = 10000;
						for (int i = 1; i < point; i++)
						{
							if (tag + i >= V)
								cout << "wrong:" << tag << "," << tag + i << ">V.\n";
							for (int j = 0; j < 2; j++)
								pathnew[s - 1][tag + i][j] = B[i][j];
						}
					}//if end 
				}//for k end

				int sign = 0;//check whether the basis
				for (int i = 0; i < E; i++)
					if (indexnew[s - 1][i][0] != 0)
						sign = 1;

				if (sign == 0)//if the basis
				{
					flag = 1; pathnum++;
					if (first == 0)
					{
						first++;
						//fout << "path:";//print the first basis
						len=PrintResult(pathnew[s - 1], V, T ,pp,table, Variable, Equation)+pp+1;
						//fout << "end\n";
					}

					int guess = 0;//record the number of guesses
					for (int i = 0; i < V; i++)
						if (pathnew[s - 1][i][1] == 10000)
							guess++;

					if (guess < guessmin)
						guessmin = guess;
				}
			}
		}//for s end

		if (flag == 1)
		{
			cout << "level:" << t << " is done.\n";
			break;//Find the basis and break out of the loop for t
		}


		for (int k = 0; k < V; k++)// update the columns in the trellis diagram
		{
			for (int i = 0; i < E; i++)
			{
				for (int j = 0; j < const1; j++)
				{
					index[k][i][j] = indexnew[k][i][j];
				}
			}
			for (int i = 0; i < V; i++)
				for (int j = 0; j < 2; j++)
					path[k][i][j] = pathnew[k][i][j];
		}
		cout << "level:" << t << " is done.\n";
	}//for t end

	for (int i = 0; i < V; i++)
	{
		for (int j = 0; j < 2; j++)
			for (int k = 0; k < 4; k++)
				fout << table[i][j][k] << " ";
		fout << "\n";
	}

	cout << guessmin << "\n";
	cout << pathnum << "\n";
}



int main()
{
	int T = 5; //number of keystream words
	int E = 120 * T + 16; //number of equations
	int V = 112 * (T + 1); //number of variables
	int M = E - 24 * T; // Equation number for different propagation rule
	int I = E - 16 * T; // Equation number for different propagation rule
	int const1 = 9; //max number of items in each equation, the first item records the number of unknown variables in the equation

	HGDalgorithm(T, M, I, E, V, const1);
	return 0;
}